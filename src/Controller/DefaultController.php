<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\DomCrawler\Crawler;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        // Initialize the HTTP client
        $client = HttpClient::create();

        // Use a distributed cache to store the data from the RSS feed and the news API
        $cache = new \Memcached();
        $cache->addServer('localhost', 11211);

        $rssData = $cache->get('rss_feed_data');
        if (!$rssData) {
            // Fetch the RSS feed data
            $rssData = file_get_contents('https://www.commitstrip.com/en/feed/');

            // Store the data in the cache for future requests
            $cache->set('rss_feed_data', $rssData);
        }

        // Parse the RSS feed data as XML
        $rssXml = simplexml_load_string($rssData);

        // Get the feed items
        $items = $rssXml->channel->item;

        // Map the items to an array of image URLs
        $imageUrls = array_map(function ($item) {
            if (is_string($item)) {
                return;
            }
            // Create a crawler for the item's content
            $crawler = new Crawler((string)$item->children('http://purl.org/rss/1.0/modules/content/', true));

            // Filter the crawler to find images
            $images = $crawler->filter('img')->extract(['src']);

            // Return the first image URL
            return $images[0] ?? '';
        }, (array)$items);

        // Remove any empty or duplicate image URLs
        $imageUrls = array_unique(array_filter($imageUrls, 'is_string'));

        // Use a distributed cache to store the data from the news API
        $newsData = $cache->get('news_api_data');
        if (!$newsData) {
            // Fetch the data from the news API
            $response = $client->request('GET', 'https://newsapi.org/v2/top-headlines', [
                'query' => [
                    'country' => 'us',
                    'apiKey' => 'a30e9f0e4e4f45c4a0a3fc6f96a3516a',
                ],
            ]);

            // Get the data as an array
            $newsData = $response->toArray();

            // Store the data in the cache for future requests
            $cache->set('news_api_data', $newsData);
        }

        // Map the articles to an array of image URLs
        $newsImageUrls = array_map(function ($article) {
            // Return the image URL if it exists
            return $article['urlToImage'] ?? '';
        }, $newsData['articles']);

        // Remove any empty or duplicate image URLs
        $newsImageUrls = array_unique(array_filter($newsImageUrls, 'is_string'));

        // Merge the image URLs from both sources
        $imageUrls = array_merge($imageUrls, $newsImageUrls);

        // Initialize the array of images
        $images = [];

        // Use a distributed message queue to offload the image processing tasks to a separate worker process
        $queue = new \RabbitMQ\Client();
        $queue->connect();

        // Iterate over the image URLs
        foreach ($imageUrls as $url) {
            if (empty($url)) {
                continue;
            }

            // Add the image processing task to the queue
            $queue->addTask(function () use ($url) {
                // Fetch the image data
                $imageData = file_get_contents($url, false, stream_context_create([
                    'ssl' => [
                        "verify_peer" => false,
                        "verify_peer_name" => false,
                    ]
                ]));

                // Return the image data
                return [
                    'stream' => $imageData,
                    'mimeType' => 'image/jpeg', // or image/png, image/gif, etc.
                ];
            });
        }

        // Wait for the worker process to complete all image processing tasks
        $queue->wait();

        // Get the results of the image processing tasks
        $images = $queue->getResults();

        // Render the template with the images data
        return $this->render('default/index.html.twig', [
            'images' => $images,
        ]);
    }
}
